## apy
A wrapper for deploying python-based APIs on PHP servers 
without the use of fcgi or any other server configuration. 
Just set your python path and go!

## setup
Clone into your public_html directory:

```
git clone https://gitlab.com/perrydc/apy
cd apy
```

If you're using a virtual environment, make sure root has access to that 
environment and specify it at the top of index.php.  Just 
navigate to **YOURDOMAIN.com/api?ep=test&foo=bar**.  You should see 
something like:

```
{
  version: "3.6.0",
  get: {
    ep: "test",
    foo: "bar"
  },
  post: {}
}
```

If you get a blank screen, you may need to give test.py execute rights, 
or you might need to update the python executable location: 

```
chmod +x test.py
which python
nano index.php
```

An alternate $venvPath might look like:

```php
$venvPath = "~/anaconda3/bin/python";
```

## Use
From anywhere within your **apy** folder or any of its subdirectories, 
create a python file that does whatever and outputs as json.  The 
**?ep=** part of the query string should match the name of the file, 
like **?ep=subdirectory/filename** (without the .py suffix).  You can 
then add on any other name value pairs that your python script requires 
into the url.  The **apy/** root file will route them to your python 
script.

If you'd like to use the api within a project, just copy the index.php 
file (and maybe the test.py example) over to your project.  No other 
configuration is necessary.
