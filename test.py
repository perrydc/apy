#!/anaconda3/bin/ python
# debug via: python test.py ep=test\&foo=bar
import sys, json, urllib.parse

rawGet = urllib.parse.unquote(sys.argv[1])
argList = rawGet.split('&')
get = {}
for arg in argList:
  kvs = arg.split('=')
  get.update({kvs[0]:kvs[1]})

if len(sys.argv)>2:
  rawPost = urllib.parse.unquote(sys.argv[2])
  post = json.loads(rawPost)
  if post:
    pass #post handler to come
  else:
    post = {}
else:
  post = {}

if len(sys.argv)>2:
  rawHead = urllib.parse.unquote(sys.argv[3])
  head = json.loads(rawHead)
else:
  head = {}

v = ("%s.%s.%s" % sys.version_info[:3])
dict = {'version':v,'get':get,'post':post,'head':head}
j = json.dumps(dict)
print(j)
